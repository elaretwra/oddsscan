from django.urls import path, include, re_path
from . import views

urlpatterns = [
    path('graphs/<int:event_id>/', views.graphs, name='graphs'),
    # path('graphs?event_id=1', views.graphs, name='graphs'),
    # re_path(r'^graphs?event\w+', views.graphs, name='graphs'),
    path('games', views.games, name='games'),
    path('games/<int:game_id>/', views.game, name='game'),
    path('api/games', views.api_games.as_view(), name='api-games'),
    path('api/games/<int:game_id>/', views.api_game.as_view(), name='api-game'),
    path('intro/', views.intro, name='intro'),
    # path('chart', views.line_chart, name='line_chart'),
    # path('chartJSON', views.line_chart_json, name='line_chart_json'),
    # path('profile/', views.profile, name='profile'),
    # path('activate/account/', user_views.activate_account, name='activate'),
    # path('register/', user_views.register, name='register'),
    # path('profile/', user_views.profile, name='profile'),
    # path('profile/', include('users.urls')),
    # path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    # path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
]