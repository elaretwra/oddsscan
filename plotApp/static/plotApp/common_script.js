
function setBkFltHtml(bkie, bks2check){
//  It creates the html code for the bookies filter

    if (bks2check.includes(bkie)){
//  The checkbox is checked
        var bkflt_html =
                `<input type="checkbox" id=` + bkie + ` name="bookmaker" value=` + bkie + ` checked>
                            <label for=`+ bkie + `>` + bkie + `</label><br>`
    }else{
//  The checkbox is not checked
        var bkflt_html =
                `<input type="checkbox" id=` + bkie + ` name="bookmaker" value=` + bkie + `>
                            <label for=`+ bkie + `>` + bkie + `</label><br>`
    }
    return bkflt_html
}


function setLeagueFilterHtml(lg_org, lg_mdf, lgs2check){
//  It creates the html code for the leagues filter

    if (lgs2check.includes(lg_mdf)){
        // The checkbox is checked
        var lg_flt_html =
                `<input type="checkbox" id=` + lg_mdf + ` name="league" value=` + lg_mdf + ` checked>
                            <label for=`+ lg_mdf + `>` + lg_org + `</label><br>`
    }else{
        // The checkbox is not checked
        var lg_flt_html =
                `<input type="checkbox" id=` + lg_mdf + ` name="league" value=` + lg_mdf + `>
                            <label for=`+ lg_mdf + `>` + lg_org + `</label><br>`
    }
    return lg_flt_html
}
