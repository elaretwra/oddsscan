
/* Link for integrating the charts and passing values via the rest framework
    https://www.youtube.com/watch?v=B4Vmm3yZPgc */


var endpoint = '/api/chart/data';
var event = [];
var bksflt_html = "";
var lgsflt_html = "";

//   Define the bookies from the url
const urlBkies = new URLSearchParams(window.location.search);
var bkies = urlBkies.getAll('bookmaker');

//   if none bkie is filtered then get them all else get the 'bkies' filtered
const bkies_all = ["stoiximan", "novibet", "betshop"];
bkies = (bkies.length == 0) ? bkies_all : bkies;

//   Define the lgs from the url
const urlLgsRaw = new URLSearchParams(window.location.search);
var lgs_url = urlLgsRaw.getAll('league');

//setInterval(test, 3000);
$(document).ready(function(){
    oddsIntervalId = setInterval(function() { fetchdata(false); }, 5000);
});

function oddsSetInterval(){
    oddsIntervalId = setInterval(function() { fetchdata(false); }, 5000);
    return oddsIntervalId
}

function oddsClearInterval(){
    clearInterval(oddsIntervalId);
}

function fetchdata(sort_btn_pressed){
// Its purpose is to be executed repeatedly called by the setInterval() or by the buttons which sort out the events
// It updates also the html code for the rakes
// The sort_btn_pressed helps us distinguish whether fetchdata() is called by the setInterval or by the buttons
// - By Rake, By KickOff, By League.
// If a button is pressed, we wanna execute the fetchdata() regardless of the play/pause mode.
// In next releases, we should update only the data.
    $.ajax({
        method: "GET",
        url: endpoint,
        success: function(data){

            //TODO: You may enclose the commands below in an if not (sort_btn_pressed)
            //TODO: aka do not update the info when sorting the events
            events = data.events
            evs_by_lg = data.evs_by_lg
            lgs_all = data.lgs
            timenow = data.time_now

            if ($("#btn-by-league").hasClass("active")) {
                events1 = evs_by_lg
            }
            else{
                events1 = events
            }

    //      Create the Html code with the proper element ids and fill them with the proper content

            document.getElementById('timenow').innerHTML = timenow;
            renderGamesDatasOnly(events1, lgs_url, bkies)

            for (i in events) {
                event = events[i]
                for (j in event) {
                    event_bk = event[j]

//                  Get the 2 last odds of each point for the specific bookie
                    bk_odds1 = event_bk['1'] // list of ints
                    bk_oddsX = event_bk['X'] // list of ints
                    bk_odds2 = event_bk['2'] // list of ints
                    bk_lastodd1 = bk_odds1[bk_odds1.length - 1]
                    bk_bfrlastodd1 = bk_odds1[bk_odds1.length - 2]
                    bk_lastoddX = bk_oddsX[bk_oddsX.length - 1]
                    bk_bfrlastoddX = bk_oddsX[bk_oddsX.length - 2]
                    bk_lastodd2 = bk_odds2[bk_odds2.length - 1]
                    bk_bfrlastodd2 = bk_odds2[bk_odds2.length - 2]

//                  Define the class (greenclass, redclass, null) regarding to the odds movement (rise, drop, steady)
                    class_odd1 = defineColorClass(bk_lastodd1, bk_bfrlastodd1)
                    class_odd2 = defineColorClass(bk_lastoddX, bk_bfrlastoddX)
                    class_odd3 = defineColorClass(bk_lastodd2, bk_bfrlastodd2)

                    // Form the odds ids in order to access and color them

                    // IDs when using queryselector cannot start with a digit. That's why we put in front the 'v'
                    // (see https://stackoverflow.com/questions/37270787/uncaught-syntaxerror-failed-to-execute-queryselector-on-document/37271406)
                    event_id = event_bk['event_key']
                    bk_ev_id = event_bk['bk_event_key']
                    div_id1 = 'v' + bk_ev_id + '_1'
                    div_id2 = 'v' + bk_ev_id + '_X'
                    div_id3 = 'v' + bk_ev_id + '_2'

//                  Assign the class dynamically via Vue.js
                    var vm = new Vue({
                                 el: '#' + div_id1,
                                 data: {
                                    color_class1: class_odd1,
                                 }
                             });
                    var vm = new Vue({
                                 el: '#' + div_id2,
                                 data: {
                                    color_class2: class_odd2,
                                 }
                             });
                    var vm = new Vue({
                                 el: '#' + div_id3,
                                 data: {
                                    color_class3: class_odd3
                                 }
                             });
                 }
            }
        },
        error: function(error_data){
            console.log("error")
            console.log(error_data)
        }
    });
}


$.ajax({
// It runs only at the first time of the page loading and it sets the html code for the filters and the rakes

    method: "GET",
    url: endpoint,
    success: function(data){
        events = data.events
        lgs_all = data.lgs

//      if none lg is filtered then get them all else get the 'lgs' filtered
        lgs_url = (lgs_url.length == 0) ? lgs_all['mdf'] : lgs_url;
        console.log(lgs_url)

//      Set the html code for the bkies filters
        for (i in bkies_all) {
            bkflt_html = setBkFltHtml(bkies_all[i], bkies)
            bksflt_html = bksflt_html + bkflt_html
        }
        document.getElementById('check-bkies').innerHTML = bksflt_html;

//      Set the html code for the lgs filters
        for (i in lgs_all['mdf']) {
            lg_org = lgs_all['org'][i]
            lg_mdf = lgs_all['mdf'][i]
            lgflt_html = setLgFltHtml(lg_org, lg_mdf, lgs_url)
            lgsflt_html = lgsflt_html + lgflt_html
        }
        document.getElementById('check-lgs').innerHTML = lgsflt_html;

//      Create the Html code with the proper element ids and fill them with the proper content
        setEventHtmlContent(events, lgs_url, bkies)
//        setInterval(function() {$('#output_box').load('api-data');}, 3000);
//            setInterval(setEventHtmlContent, 3000, events, lgs_url, bkies);
    },
    error: function(error_data){
        console.log("error")
        console.log(error_data)
    }
});

function renderGamesDatasOnly(events, lgs_url, bkies){
// It renders only the odds and rakes. Elements identified by id.

    for (i in events) {
        event = events[i]
        // lg var on the same format with lgs_url. Not here, bad implementation.
        lg = event[0]['league'].replace(/\s/g, '')

        for (i in event){
            event_bk = event[i]
            bk = event[i]['bookmaker']
            bk_ev_id = event[i]['bk_event_key']
            if (bkies.includes(bk)){

                // Get the last odds (1X2) for the current bookie

                bk_odds1 = event_bk['1'] // list of ints
                bk_oddsX = event_bk['X'] // list of ints
                bk_odds2 = event_bk['2'] // list of ints
                bk_lastodd1 = bk_odds1[bk_odds1.length - 1]
                bk_lastoddX = bk_oddsX[bk_oddsX.length - 1]
                bk_lastodd2 = bk_odds2[bk_odds2.length - 1]

                // Calculate the rake
                rake_float = (1/bk_lastodd1 + 1/bk_lastoddX + 1/bk_lastodd2 - 1) * 100
                rake = Math.round(rake_float * 10)/ 10

                // Render only the odds and the rake
                document.getElementById('v' + bk_ev_id + '_1').innerHTML = bk_lastodd1
                document.getElementById('v' + bk_ev_id + '_X').innerHTML = bk_lastoddX
                document.getElementById('v' + bk_ev_id + '_2').innerHTML = bk_lastodd2
                document.getElementById('v' + bk_ev_id + '_rake').innerHTML = rake

            }

            // Render only the max odds and the min-rake

            ans_raw = findMaxOdds(event)
            max_odds_ev = ans_raw[0]
            rake_ev = ans_raw[1]

            ev_id = event[0]['event_key']
            document.getElementById('v' + ev_id + '_max1').innerHTML = max_odds_ev['odds'][0]
            document.getElementById('v' + ev_id + '_maxX').innerHTML = max_odds_ev['odds'][1]
            document.getElementById('v' + ev_id + '_max2').innerHTML = max_odds_ev['odds'][2]
            document.getElementById('v' + ev_id + '_minrake').innerHTML = rake_ev
        }
    }
    return
}

function expandAccordions(){
// It expands all the collapsed accordions within the middle box

    // Get a NodeList of all collapsed accordions in the middle box
    const games = document.querySelector("div[id='middlebox']");
    const collapsedClasses = games.querySelectorAll("div[class='collapse']");

    // Change the class and make it that of an expanded accordion
    collapsedClasses.forEach(element => {
      element.className = 'collapse show';
    });

    return
}

function collapseAccordions(){
// It collapses all the expanded accordions within the middle box

    // Get a NodeList of all expanded accordions in the middle box
    const games = document.querySelector("div[id='middlebox']");
    const expandedClasses = games.querySelectorAll("div[class='collapse show']");

    // Change the class and make it that of a collapsed accordion
    expandedClasses.forEach(element => {
      element.className = 'collapse';
    });

    return
}

function setEventHtmlContent(events, lgs_url, bkies){
// It creates the Html code with the proper element ids and it fills them with the proper content

//  Set the html code for all the events
    var evs_html_list = [];
    var max_odds = [];
    var rakes = [];
    for (i in events) {
        event = events[i]
//      lg var on the same format with lgs_url. Not here, bad implementation.
        lg = event[0]['league'].replace(/\s/g, '')

        ans_raw = setEventHtml(lgs_url, lg, event, bkies)
        event_html = ans_raw[0]
        max_odds_ev = ans_raw[1]
        rake_ev = ans_raw[2]

        evs_html_list.push(event_html)
        max_odds.push(max_odds_ev)
        rakes.push(rake_ev)
    }

//  We need the unsorted max_odds, rakes in the setEventsMaxOdds()
    max_odds_unsrt = max_odds.slice();
    rakes_unsrt = rakes.slice();

    if ($("#btn-by-rake").hasClass("active")) {

        var sorted = sortByFirstArg(rakes, max_odds, evs_html_list)
        for (var i = 0; i < sorted.length; i++) {
            rakes[i] = sorted[i][0];
            max_odds[i] = sorted[i][1];
            evs_html_list[i] = sorted[i][2];
        }
    }

//  Concatenate the strings list.toString() does not work perfectly because it converts also the splitting comma
//  to string.
    events_html = ""
    for (i in evs_html_list){
//    for (var i = 0; i < evs_html_list.length; i++){
        events_html += evs_html_list[i]
    }
    document.getElementById("events").innerHTML = events_html

//  Fill in the span elms of the generated html code above with the proper odds
//    var cnt = -1
//    for (i in events) {
//        event = events[i]
//        cnt += 1
////      lg var on the same format with lgs_url. Not here, bad implementation.
//        lg = event[0]['league'].replace(/\s/g, '')
//        event_id = event[0]['event_key']
//        if (lgs_url.includes(lg)){
////          Be careful the title_id, chart_id<1,2,3> generation to match with those ones of the above for loop
//            title_id = 'title_' + event_id
//            div_id0 = 'bookie_' + event_id
//            div_id1 = 'odd1_' + event_id
//            div_id2 = 'odd2_' + event_id
//            div_id3 = 'odd3_' + event_id
//            div_id4 = 'rake_' + event_id
//
//            setEventMaxOdds(div_id0, div_id1, div_id2, div_id3, div_id4, max_odds_unsrt[cnt], rakes_unsrt[cnt])
//        }
//    }
    return
}


function setEventHtml(lgs_url, lg, event, bkies){
//      It creates the Html code with the proper element ids

    /* Create as many divs (with an id each) as the #bookmakers for the specific event
    E.g. for an event with 2 bookies it will create 8 divs, 4 for each bookie. The first
    3 are for the odds while the last one is for the rake */

    var event_html = ""
    if (lgs_url.includes(lg)){
        title_elm =
                    '<div style="background: #5f788a; color: #ffffff"><span>' + event[0]['team1_id'] + ' vs ' + event[0]['team2_id'] + '</span>' +
                    '<span class="align-right">' + event[0]['in'] + ' | ' + event[0]['league'] + '</span></div>'
//                    '<span class="align-right"><a title="odds history" href="graphs/' +
//                    event[0]['event_key'] + '"><i class="fa fa-line-chart"></i></a></span>';

//        div_elms0 = "<div id=" + div_id0 + "_max></div>"
//        div_elms1 = "<div id=" + div_id1 + "_max></div>"
//        div_elms2 = "<div id=" + div_id2 + "_max></div>"
//        div_elms3 = "<div id=" + div_id3 + "_max></div>"
//        div_elms4 = "<div id=" + div_id4 + "_min></div>"
        div_elms = ''
        apply_color = false
        for (i in event){
            event_bk = event[i]
            bk = event[i]['bookmaker']
            bk_ev_id = event[i]['bk_event_key']
            if (bkies.includes(bk)){

    //          Get the last odds (1X2) for the current bookie

                bk_odds1 = event_bk['1'] // list of ints
                bk_oddsX = event_bk['X'] // list of ints
                bk_odds2 = event_bk['2'] // list of ints
                bk_lastodd1 = bk_odds1[bk_odds1.length - 1]
                bk_lastoddX = bk_oddsX[bk_oddsX.length - 1]
                bk_lastodd2 = bk_odds2[bk_odds2.length - 1]

    //          Calculate the rake
                rake_float = (1/bk_lastodd1 + 1/bk_lastoddX + 1/bk_lastodd2 - 1) * 100
                rake = Math.round(rake_float * 10)/ 10

                content0 = '<img class="side_logo" src="/static/plotApp/images/'+ bk +'.png">';
                content1 = bk_lastodd1;
                content2 = bk_lastoddX;
                content3 = bk_lastodd2;
                content4 = rake;

                div_elm0 = '<div class="box-cell"> <div id="v' + bk_ev_id + '_logo" class="bookie_logo">' + content0 + '</div></div>'
                div_elm1 = '<div class="box-cell"> <div id="v' + bk_ev_id + '_1" class="color_class1">' + content1 + '</div></div>'
                div_elm2 = '<div class="box-cell"> <div id="v' + bk_ev_id + '_X" class="color_class2">' + content2 + '</div></div>'
                div_elm3 = '<div class="box-cell"> <div id="v' + bk_ev_id + '_2" class="color_class2">' + content3 + '</div></div>'
                div_elm4 = '<div class="box-cell"> <div id="v' + bk_ev_id + '_rake">' + content4 + '</div></div>'

                if (apply_color){
                    style_color = 'style="background-color: #d2f8d2"'
                }else{
                    style_color = ''
                }
                apply_color = !apply_color

                div_elms += '<div id="' + bk_ev_id + '" class="box-row" '+ style_color +'>' + div_elm0 + div_elm1 + div_elm2 + div_elm3 + div_elm4 + '</div>'
            }
        }

        // Add an extra box-row for the max odds and min rake

        ans_raw = findMaxOdds(event)
        max_odds_ev = ans_raw[0]
        rake_ev = ans_raw[1]

        ev_id = event[0]['event_key']
        div_empty = '<div class="box-cell"></div>'
        div_max1 = '<div class="box-cell"> <span id="v' + ev_id + '_max1" class="color_class1">' + max_odds_ev['odds'][0] + '</span><img class="top_logo" src="/static/plotApp/images/'+ max_odds_ev['bkies'][0] +'.png">' + '</div>'
        div_maxX = '<div class="box-cell"> <span id="v' + ev_id + '_maxX" class="color_class1">' + max_odds_ev['odds'][1] + '</span><img class="top_logo" src="/static/plotApp/images/'+ max_odds_ev['bkies'][1] +'.png">' + '</div>'
        div_max2 = '<div class="box-cell"> <span id="v' + ev_id + '_max2" class="color_class1">' + max_odds_ev['odds'][2] + '</span><img class="top_logo" src="/static/plotApp/images/'+ max_odds_ev['bkies'][2] +'.png">' + '</div>'
        div_minrake = '<div class="box-cell"> <div id="v' + ev_id + '_minrake" class="color_class1">' + rake_ev + '</div></div>'

        div_maxodds = '<div class="box-row">' + div_empty + div_max1 + div_maxX + div_max2 + div_minrake + '</div>'

        odds_html =
                `<div class="card">
                    <div class="card-header" style="padding: 0" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-light" style="padding: 0" type="button" data-toggle="collapse" data-target="#collapse_` + ev_id + `" aria-expanded="true" aria-controls="collapseOne">
                                ` + div_maxodds + `
                            </button>
                        </h5>
                    </div>

                    <div id="collapse_` + ev_id + `" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body" style="padding: 0">
                            ` + div_elms + `
                        </div>
                    </div>
                </div>`


        // Integrate javascript to html, see https://www.w3schools.com/js/js_functions.asp
        event_html =
                `
                <div class="media">
                    <div id=` + ev_id + ` class="event" style="padding: 0">
                        ` + title_elm + `
                        <div class="box">
                            ` + odds_html + `
                        </div>
                    </div>
                </div>`
    }
    return [event_html, max_odds_ev, rake_ev]
}


function findMaxOdds(event){
    var max_odds_ev = {'odds': [-1, -1, -1], 'bkies': [null, null, null]}
    for (i in event){
        event_bk = event[i]
        bk = event[i]['bookmaker']
        bk_ev_id = event[i]['bk_event_key']
        if (bkies.includes(bk)){

//          Get the last odds (1X2) for the current bookie

            bk_odds1 = event_bk['1'] // list of ints
            bk_oddsX = event_bk['X'] // list of ints
            bk_odds2 = event_bk['2'] // list of ints
            bk_lastodd1 = bk_odds1[bk_odds1.length - 1]
            bk_lastoddX = bk_oddsX[bk_oddsX.length - 1]
            bk_lastodd2 = bk_odds2[bk_odds2.length - 1]

            if (bk_lastodd1 > max_odds_ev['odds'][0]){
                max_odds_ev['odds'][0] = bk_lastodd1
                max_odds_ev['bkies'][0] = bk
            }
//            else if (bk_odds1 == max_odds_ev['odds'][0]){
//            }
            if (bk_lastoddX > max_odds_ev['odds'][1]){
                max_odds_ev['odds'][1] = bk_lastoddX
                max_odds_ev['bkies'][1] = bk
            }
            if (bk_lastodd2 > max_odds_ev['odds'][2]){
                max_odds_ev['odds'][2] = bk_lastodd2
                max_odds_ev['bkies'][2] = bk
            }

        }
    }

//          Calculate the rake
    rake_ev_float = (1/max_odds_ev['odds'][0] + 1/max_odds_ev['odds'][1] + 1/max_odds_ev['odds'][2] - 1) * 100
    rake_ev = Math.round(rake_ev_float * 10)/ 10

    return [max_odds_ev, rake_ev]

}

function setEventMaxOdds(div_id0, div_id1, div_id2, div_id3, div_id4, max_odds_ev, rake_ev){
//  It assigns in the generated html code the max odds and the rake for the specific event

    document.getElementById(div_id0 + '_max').innerHTML = 'best odds';
    document.getElementById(div_id1 + '_max').innerHTML = max_odds_ev['odds'][0] + '<span class="align-right">&nbsp;' + max_odds_ev['bkies'][0] + '</span>';
    document.getElementById(div_id2 + '_max').innerHTML = max_odds_ev['odds'][1] + '<span class="align-right">&nbsp;' + max_odds_ev['bkies'][1] + '</span>';
    document.getElementById(div_id3 + '_max').innerHTML = max_odds_ev['odds'][2] + '<span class="align-right">&nbsp;' + max_odds_ev['bkies'][2] + '</span>';
    document.getElementById(div_id4 + '_min').innerHTML = rake_ev;

}


function sortByFirstArg(toSort1, toSort2, toSort3) {
//  It accepts 3 arguments-arrays and it sorts them all in ascending order based on the 1st argument
//  toSort = [null]*toSort1.length

//  src1: https://stackoverflow.com/questions/3730510/javascript-sort-array-and-return-an-array-of-indicies-that-indicates-the-positi
//  src2: https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_sort2
//  sort() documentation: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort

//  Collect all the Arrays to one Array, 'toSort'.
    toSort = []
    toSort.length = toSort1.length
    for (var i = 0; i < toSort.length; i++) {
        toSort[i] = [toSort1[i], toSort2[i], toSort3[i]];
    }

//  Sort the Array 'toSort' by the first Array 'toSort1'.
    toSort.sort(function(a, b) {
        return a[0] - b[0];
    });

    return toSort;
}


function defineColorClass(last_odd, bfrlast_odd) {
//  It finds out the class to assign depending on the odd movement.
//  odd rise -> greenclass
//  odd drop -> redclass
//  odd steady -> null

    if (last_odd == bfrlast_odd) {
        color_class = null // "none" works also. Check again the optimal solution.
    }else if (last_odd > bfrlast_odd){
        color_class = 'greenclass'
    }else{ // bk_lastodd1 < bk_bfrlastodd1
        color_class = 'redclass'
    }

    return color_class
}


var vm = new Vue({
             el: '#toolBtns',
             data: {
                activeBtn:''
             },

             methods: {
                sortby: function (msg) {
//                    window.location.reload();
                    fetchdata(true)
                }
              }
         });

var vm = new Vue({
             el: '#collapse_expand',
             data: {
             },

             methods: {
                collapse_expand: function (msg) {
                    if (this.isActive){
                        collapseAccordions();
                    }else{
                        expandAccordions();
                    }
                    this.isActive = !this.isActive;
                }
              }
         });

var vm = new Vue({
             el: '#run_pause',
             data: {
             },

             methods: {
                run_pause: function (msg) {
                    this.isActive = !this.isActive;
                    if (this.isActive){
                        oddsClearInterval(oddsIntervalId);
                    }else{
                        oddsIntervalId = oddsSetInterval();
                    }
                }
              }
         });


function logSubmit(event) {
//    event.preventDefault();
    location.reload();
}

const form = document.getElementById('form');
form.addEventListener('submit', logSubmit);
