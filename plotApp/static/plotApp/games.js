
/* Link for integrating the charts and passing values via the rest framework
    https://www.youtube.com/watch?v=B4Vmm3yZPgc */


var endpoint = '/api/games';
var event = [];
var bksflt_html = "";


//   Define the bookies from the url
//const urlBkies = new URLSearchParams(window.location.search);
//var bkies = urlBkies.getAll('bookmaker');

//   if none bkie is filtered then get them all else get the 'bkies' filtered
//const bkies_all = ["stoiximan", "novibet", "betshop"];
//bkies = (bkies.length == 0) ? bkies_all : bkies;

////setInterval(test, 3000);
//$(document).ready(function(){
//    oddsIntervalId = setInterval(function() { fetchdata(false); }, 5000);
//});
//
oddsSetInterval();
function oddsSetInterval(){
    oddsIntervalId = setInterval(function() { fetchdata(false); }, 5000);
    return oddsIntervalId
}

function oddsClearInterval(){
    clearInterval(oddsIntervalId);
}

function fetchdata(sort_btn_pressed){
// Its purpose is to be executed repeatedly called by the setInterval() or by the buttons which sort out the events
// It updates also the html code for the rakes
// The sort_btn_pressed helps us distinguish whether fetchdata() is called by the setInterval or by the buttons
// - By Rake, By KickOff, By League.
// If a button is pressed, we wanna execute the fetchdata() regardless of the play/pause mode.
// In next releases, we should update only the data.
    $.ajax({
        method: "GET",
        url: endpoint,
        success: function(data){

            //TODO: You may enclose the commands below in an if not (sort_btn_pressed)
            //TODO: aka do not update the info when sorting the events
            games = data.games

            document.getElementById('timenow').innerHTML = 'unknown';

            // Update only the necessary attributes (odds, logo, color movement)
            renderGamesDatasOnly(games)

        },
        error: function(error_data){
            console.log("error")
            console.log(error_data)
        }
    });
}

render_html(false);
function render_html(sort_btn_pressed){
    $.ajax({
    // It runs only at the first time of the page loading and it sets the html code for the filters and the rakes

        method: "GET",
        url: endpoint,
        success: function(data){
            // Get the games and leagues from the api
            games = data.games
            leagues = data.leagues

            // Get the leagues -in slug format- from the url
            const urlLgsRaw = new URLSearchParams(window.location.search);
            var lgs_slug_url = urlLgsRaw.getAll('league');

            // Create the leagues from url (lgs_url) and leagues-slug from url (lgs_slug_url)
            result = createLeaguesFromUrl(lgs_slug_url, leagues)
            var lgs_url = result[0];
            var lgs_slug_url = result[1];

            // Set the html code for the lgs filters
            var lgs_flt_html = "";
            for (i in leagues) {
                lg = leagues[i]
                lg_flt_html = setLeagueFilterHtml(lg['league_id'], lg['league_id_slug'], lgs_slug_url)
                lgs_flt_html = lgs_flt_html + lg_flt_html
            }
            document.getElementById('check-lgs').innerHTML = lgs_flt_html;

            // Set the Html code for the games (only for the selected lgs_url)
            setGameHtmlContent(games, lgs_url)
        },
        error: function(error_data){
            console.log("error")
            console.log(error_data)
        }
    });
}

function createLeaguesFromUrl(lgs_slug_url, leagues_api){

    var lgs_url = [];
    if (lgs_slug_url.length != 0){
        var lgs_url = [];
        for (i in lgs_slug_url) {
            lg_slug_url = lgs_slug_url[i]
            found = false

            // Check if the league-slug from the url exists in the api leagues
            for (i in leagues_api) {
                lg_slug = leagues_api[i]["league_id_slug"]
                if (lg_slug_url == lg_slug){
                    lg = leagues_api[i]["league_id"]
                    found = true
                    break;
                }
            }

            if (found){
                // Update the lgs_url with the original leagues
                lgs_url.push(lg)
            }
        }
    }
    else{
        // if none league is selected in the filter then get them all

        // lgs_slug_url gets all league_id_slug from api
        // lgs_url gets all league_id from api
        var lgs_slug_url = []
        var lgs_url = []
        for (i in leagues_api){
            lgs_slug_url.push(leagues_api[i]["league_id_slug"])
            lgs_url.push(leagues_api[i]["league_id"])
        }
    }

    return [lgs_url, lgs_slug_url]
}


function renderGamesDatasOnly(games){
// It renders only the odds and rakes. Elements identified by id.

    for (i in games) {
        game = games[i]

        game_id = game['game_key']
        if (document.getElementById('v' + game_id + '_max1') != null){
            // Change color class based on the odd movement
            updateColorClass('v' + game_id + '_max1', game['max_odd1'], parseFloat(document.getElementById('v' + game_id + '_max1').innerHTML))

            // Update with the odd value and sportsbook logo
            document.getElementById('v' + game_id + '_max1').innerHTML = game['max_odd1']
            document.getElementById('v' + game_id + '_logo1').src = '/static/plotApp/images/' + game['max_bkie1'] +'.png';
        }
        if (document.getElementById('v' + game_id + '_maxX') != null){
            // Change color class based on the odd movement
            updateColorClass('v' + game_id + '_maxX', game['max_oddX'], parseFloat(document.getElementById('v' + game_id + '_maxX').innerHTML))

            // Update with the odd value and sportsbook logo
            document.getElementById('v' + game_id + '_maxX').innerHTML = game['max_oddX']
            document.getElementById('v' + game_id + '_logoX').src = '/static/plotApp/images/' + game['max_bkieX'] +'.png';
        }
        if (document.getElementById('v' + game_id + '_max2') != null){
            // Change color class based on the odd movement
            updateColorClass('v' + game_id + '_max2', game['max_odd2'], parseFloat(document.getElementById('v' + game_id + '_max2').innerHTML))

            // Update with the odd value and sportsbook logo
            document.getElementById('v' + game_id + '_max2').innerHTML = game['max_odd2']
            document.getElementById('v' + game_id + '_logo2').src = '/static/plotApp/images/' + game['max_bkie2'] +'.png';
        }
        if (document.getElementById('v' + game_id + '_minrake') != null){
            document.getElementById('v' + game_id + '_minrake').innerHTML = game['rake']
        }
    }
    return
}


function setGameHtmlContent(games, selected_lgs){
// It creates the Html code with the proper element ids and it fills them with the proper content

    // Create a list of all games (html format)
    var games_html_list = [];
    var rakes = [];
    for (i in games) {
        game = games[i]

        if (selected_lgs.includes(game['league_id'])){
            game_html = setGameHtml(game)

            games_html_list.push(game_html)
            rakes.push(game['rake'])
        }
    }

    // Sort by rake if requested
    if ($("#btn-by-rake").hasClass("active")) {

        var sorted = sortByFirstArg(rakes, games_html_list)
        for (var i = 0; i < sorted.length; i++) {
            rakes[i] = sorted[i][0];
            games_html_list[i] = sorted[i][1];
        }
    }

    // Concatenate the games and set it in the html document
    games_html = ""
    for (i in games_html_list){
        games_html += games_html_list[i]
    }
    document.getElementById("events").innerHTML = games_html

    return
}


function setGameHtml(game){
// It creates the Html code with the proper element ids

    var game_html = ""
//    if (lgs_url.includes(game['league_id'])){
        game_id = game['game_key']
        title_elm ='<div style="background: #5f788a; color: #ffffff"><a href="/games/' + game_id + '">' + game['team1_id'] + ' vs ' + game['team2_id'] + '</a>' +
                    '<span class="align-right">' + game['league_id'] + '</span></div>'

        div_empty = '<div class="box-cell"></div>'
        div_max1 = '<div class="box-cell"> <span id="v' + game_id + '_max1" class="null">' + game['max_odd1'] +
                    '</span><img id="v' + game_id + '_logo1" class="top_logo" src="/static/plotApp/images/'+ game['max_bkie1'] +'.png">' + '</div>'
        div_maxX = '<div class="box-cell"> <span id="v' + game_id + '_maxX" class="null">' + game['max_oddX'] +
                    '</span><img id="v' + game_id + '_logoX" class="top_logo" src="/static/plotApp/images/'+ game['max_bkieX'] +'.png">' + '</div>'
        div_max2 = '<div class="box-cell"> <span id="v' + game_id + '_max2" class="null">' + game['max_odd2'] +
                    '</span><img id="v' + game_id + '_logo2" class="top_logo" src="/static/plotApp/images/'+ game['max_bkie2'] +'.png">' + '</div>'
        div_minrake = '<div class="box-cell"> <div id="v' + game_id + '_minrake">' + game['rake'] + '</div></div>'

        div_maxodds = '<div class="box-row">' + div_empty + div_max1 + div_maxX + div_max2 + div_minrake + '</div>'

        odds_html = div_maxodds

        // Integrate javascript to html, see https://www.w3schools.com/js/js_functions.asp
        game_html =
                `
                <div class="media">
                    <div id=` + game_id + ` class="event" style="padding: 0">
                        ` + title_elm + `
                        <div class="box">
                            ` + odds_html + `
                        </div>
                    </div>
                </div>`
//    }
    return game_html
}


function sortByFirstArg(toSort1, toSort2) {
//  It accepts 2 arguments-arrays and it sorts them all in ascending order based on the 1st argument
//  toSort = [null]*toSort1.length

//  src1: https://stackoverflow.com/questions/3730510/javascript-sort-array-and-return-an-array-of-indicies-that-indicates-the-positi
//  src2: https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_sort2
//  sort() documentation: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort

    //  Collect all the Arrays to one Array, 'toSort'.
    toSort = []
    toSort.length = toSort1.length
    for (var i = 0; i < toSort.length; i++) {
        toSort[i] = [toSort1[i], toSort2[i]];
    }

    //  Sort the Array 'toSort' by the first Array 'toSort1'.
    toSort.sort(function(a, b) {
        return a[0] - b[0];
    });

    return toSort;
}


function updateColorClass(tag_id, last_odd, bfrlast_odd) {
//  It finds out the class to assign depending on the odd movement.
//  odd rise -> greenclass
//  odd drop -> redclass
//  odd steady -> null

    if (last_odd == bfrlast_odd) {
        color_class = null // "none" works also. Check again the optimal solution.
    }else if (last_odd > bfrlast_odd){
        color_class = 'greenclass'
    }else{ // bk_lastodd1 < bk_bfrlastodd1
        color_class = 'redclass'
    }

    document.getElementById(tag_id).className = color_class;

    return
}


var vm = new Vue({
             el: '#toolBtns',
             data: {
                activeBtn:''
             },

             methods: {
                sortby: function (msg) {
//                    window.location.reload();
                    render_html(true)
                }
              }
         });



function logSubmit(event) {
//    event.preventDefault();
    location.reload();
}

const form = document.getElementById('form');
form.addEventListener('submit', logSubmit);
