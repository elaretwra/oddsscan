
/* Link for integrating the charts and passing values via the rest framework
    https://www.youtube.com/watch?v=B4Vmm3yZPgc */

// TODO: Not the best implementation to get the game-id. We should pass it directly from the template.
// Get the game-key (id) from the url
let url = window.location.href;
if (url.substr(url.length - 1) == '/'){
    url = url.substring(0, url.length - 1);
}
var game_id_i = url.indexOf('/games/') + 7;
var game_id = url.substring(game_id_i, url.length);

// Create the proper endpoint based on the game-key
var endpoint = '/api/games/' + game_id;

oddsSetInterval();
function oddsSetInterval(){
    oddsIntervalId = setInterval(function() { fetchdata(false); }, 5000);
    return oddsIntervalId
}

function oddsClearInterval(){
    clearInterval(oddsIntervalId);
}

function fetchdata(sort_btn_pressed){
// Its purpose is to be executed repeatedly called by the setInterval() or by the buttons which sort out the events
// It updates also the html code for the rakes
// The sort_btn_pressed helps us distinguish whether fetchdata() is called by the setInterval or by the buttons
// - By Rake, By KickOff, By League.
// If a button is pressed, we wanna execute the fetchdata() regardless of the play/pause mode.
// In next releases, we should update only the data.
    $.ajax({
        method: "GET",
        url: endpoint,
        success: function(data){

            //TODO: You may enclose the commands below in an if not (sort_btn_pressed)
            //TODO: aka do not update the info when sorting the events
            game_bkies = data.game

            document.getElementById('timenow').innerHTML = 'unknown';

            // Update only the necessary attributes (odds, color movement)
            renderGamesDatasOnly(game_bkies)

        },
        error: function(error_data){
            console.log("error")
            console.log(error_data)
        }
    });
}

render_html(false);
function render_html(sort_btn_pressed){
    $.ajax({
    // It runs only at the first time of the page loading and it sets the html code for the filters and the rakes

        method: "GET",
        url: endpoint,
        success: function(data){
            // Get the game from the api
            // TODO: Correct the api
            game_bkies = data.game

            // Set the Html code for the games (only for the selected lgs_url)
            setGameHtmlContent(game_bkies)
        },
        error: function(error_data){
            console.log("error")
            console.log(error_data)
        }
    });
}


function renderGamesDatasOnly(game_bkies){
// It renders only the odds and rakes. Elements identified by id.

    for (i in game_bkies) {
        game_bk = game_bkies[i]

        bkie = game_bk['bookmaker']
        bk_game_id = game_bk['bkie_game_key']

        tag_id = 'v' + bk_game_id + '_' + bkie + '_1'
        if (document.getElementById(tag_id) != null){
            // Change color class based on the odd movement
            updateColorClass(tag_id, game_bk['odd1'], parseFloat(document.getElementById(tag_id).innerHTML))

            // Update with the odd value
            document.getElementById(tag_id).innerHTML = game_bk['odd1']
        }

        tag_id = 'v' + bk_game_id + '_' + bkie + '_X'
        if (document.getElementById(tag_id) != null){
            // Change color class based on the odd movement
            updateColorClass(tag_id, game_bk['oddX'], parseFloat(document.getElementById(tag_id).innerHTML))

            // Update with the odd value
            document.getElementById(tag_id).innerHTML = game_bk['oddX']
        }

        tag_id = 'v' + bk_game_id + '_' + bkie + '_2'
        if (document.getElementById(tag_id) != null){
            // Change color class based on the odd movement
            updateColorClass(tag_id, game_bk['odd2'], parseFloat(document.getElementById(tag_id).innerHTML))

            // Update with the odd value
            document.getElementById(tag_id).innerHTML = game_bk['odd2']
        }

        tag_id = 'v' + bk_game_id + '_' + bkie + '_rake'
        if (document.getElementById(tag_id) != null){
            rake_float = (1/game_bk['odd1'] + 1/game_bk['oddX'] + 1/game_bk['odd2'] - 1) * 100
            rake = Math.round(rake_float * 10)/ 10
            document.getElementById(tag_id).innerHTML = rake
        }
    }
    return
}


function setGameHtmlContent(game_bkies){
// It creates the Html code with the proper element ids and it fills them with the proper content

    // Create a list of all games (html format)
    var games_html_list = [];
    var rakes = [];

    game_html = setGameHtml(game_bkies)

    // Sort by rake if requested
//    if ($("#btn-by-rake").hasClass("active")) {
//
//        var sorted = sortByFirstArg(rakes, games_html_list)
//        for (var i = 0; i < sorted.length; i++) {
//            rakes[i] = sorted[i][0];
//            games_html_list[i] = sorted[i][1];
//        }
//    }

    // Concatenate the games and set it in the html document
//    games_html = ""
//    for (i in games_html_list){
//        games_html += games_html_list[i]
//    }
    document.getElementById("events").innerHTML = game_html

    return
}


function setGameHtml(game_bkies){
// It creates the Html code with the proper element ids

    var game_html = ""
    apply_color = false
    div_elms = ''
    title_elm =
            '<div style="background: #5f788a; color: #ffffff"><span>' + game_bkies[0]['team1_id'] + ' vs ' + game_bkies[0]['team2_id'] + '</span>' +
            '<span class="align-right">' + game_bkies[0]['league_id'] + '</span></div>'
    for (i in game_bkies){
        game_bk = game_bkies[i]
        bk_game_id = game_bk['bkie_game_key']

        // Calculate the rake
        rake_float = (1/game_bk['odd1'] + 1/game_bk['oddX'] + 1/game_bk['odd2'] - 1) * 100
        rake = Math.round(rake_float * 10)/ 10

        bkie = game_bk['bookmaker']
        content0 = '<img class="side_logo" src="/static/plotApp/images/'+ bkie +'.png">';

        div_elm0 = '<div class="box-cell"> <div id="v' + bk_game_id + '_' + bkie + '_logo" class="bookie_logo">' + content0 + '</div></div>'
        div_elm1 = '<div class="box-cell"> <div id="v' + bk_game_id + '_' + bkie + '_1" class="null">' + game_bk['odd1'] + '</div></div>'
        div_elm2 = '<div class="box-cell"> <div id="v' + bk_game_id + '_' + bkie + '_X" class="null">' + game_bk['oddX'] + '</div></div>'
        div_elm3 = '<div class="box-cell"> <div id="v' + bk_game_id + '_' + bkie + '_2" class="null">' + game_bk['odd2'] + '</div></div>'
        div_elm4 = '<div class="box-cell"> <div id="v' + bk_game_id + '_' + bkie + '_rake">' + rake + '</div></div>'

        if (apply_color){
            style_color = 'style="background-color: #d2f8d2"'
        }else{
            style_color = ''
        }
        apply_color = !apply_color

        div_elms += '<div id="' + bk_game_id + '" class="box-row" '+ style_color +'>' + div_elm0 + div_elm1 + div_elm2 + div_elm3 + div_elm4 + '</div>'
    }

        odds_html = div_elms

        // Integrate javascript to html, see https://www.w3schools.com/js/js_functions.asp
        game_html =
                `
                <div class="media">
                    <div id=single-` + game_bk['game_key'] + ` class="event" style="padding: 0">
                        ` + title_elm + `
                        <div class="box">
                            ` + odds_html + `
                        </div>
                    </div>
                </div>`
//    }
    return game_html
}


function sortByFirstArg(toSort1, toSort2) {
//  It accepts 2 arguments-arrays and it sorts them all in ascending order based on the 1st argument
//  toSort = [null]*toSort1.length

//  src1: https://stackoverflow.com/questions/3730510/javascript-sort-array-and-return-an-array-of-indicies-that-indicates-the-positi
//  src2: https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_sort2
//  sort() documentation: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort

    //  Collect all the Arrays to one Array, 'toSort'.
    toSort = []
    toSort.length = toSort1.length
    for (var i = 0; i < toSort.length; i++) {
        toSort[i] = [toSort1[i], toSort2[i]];
    }

    //  Sort the Array 'toSort' by the first Array 'toSort1'.
    toSort.sort(function(a, b) {
        return a[0] - b[0];
    });

    return toSort;
}


function updateColorClass(tag_id, last_odd, bfrlast_odd) {
//  It finds out the class to assign depending on the odd movement.
//  odd rise -> greenclass
//  odd drop -> redclass
//  odd steady -> null

    if (last_odd == bfrlast_odd) {
        color_class = null // "none" works also. Check again the optimal solution.
    }else if (last_odd > bfrlast_odd){
        color_class = 'greenclass'
    }else{ // bk_lastodd1 < bk_bfrlastodd1
        color_class = 'redclass'
    }

    document.getElementById(tag_id).className = color_class;

    return
}


var vm = new Vue({
             el: '#toolBtns',
             data: {
                activeBtn:''
             },

             methods: {
                sortby: function (msg) {
//                    window.location.reload();
                    render_html(true)
                }
              }
         });



function logSubmit(event) {
//    event.preventDefault();
    location.reload();
}

const form = document.getElementById('form');
form.addEventListener('submit', logSubmit);
