
/* Link for integrating the charts and passing values via the rest framework
    https://www.youtube.com/watch?v=B4Vmm3yZPgc */

var endpoint = '/api/chart/data';
var event = [];
var events_html = "";

const bkies_all = ["stoiximan", "novibet", "betshop"];


$(document).ready(function(){
    setInterval(fetchdata, 5000);
});

function fetchdata(){
// Its purpose is to be executed repeatedly called by the setInterval() and update
// the html code for the rakes
// In next releases, we should update only the data i.e. the chart line.
    $.ajax({
        method: "GET",
        url: endpoint,
        success: function(data){
            events = data.events;
            timenow = data.time_now;

            for (i in events){
                event = events[i]
                if (event[0]['event_key'] == event_id)
                    break;
            }
//            console.log(event)
    //      Create the Html code with the proper element ids and fill them with the proper content
            document.getElementById('timenow').innerHTML = timenow;
            setEventHtmlContent(event, bkies_all)
        },
        error: function(error_data){
            console.log("error")
            console.log(error_data)
        }
    });
}


$.ajax({
    method: "GET",
    url: endpoint,
    success: function(data){
            events = data.events;
            timenow = data.time_now;

            for (i in events){
                event = events[i]
                if (event[0]['event_key'] == event_id)
                    break;
            }
//      Create the Html code with the proper element ids and fill them with the proper content
        setEventHtmlContent(event, bkies_all)
    },
    error: function(error_data){
        console.log("error")
        console.log(error_data)
    }
})

function setEventHtmlContent(event, bkies){

//      Set the html code for the event
//          lg var on the same format with lgs_url. Not here, bad implementation.


//      Fill in the canvas of the generated html code above with the appropriate charts

    /* Set event title
    event[0]['in'], event[0]['league'] is a bad implementation since the ref bookie may not be the 1st elm*/
    document.getElementById('event_title').innerHTML =
                                        '<span class="text-muted">' + event[0]['in'] + ' | ' + event[0]['league'] + '</span>' +
                                        '<p>' + event[0]['team1_id'] + ' vs ' + event[0]['team2_id'] + '</p>' ;

    setChart('myChart1', event, '1', bkies)
    setChart('myChart2', event, 'X', bkies)
    setChart('myChart3', event, '2', bkies)
}


function setChart(chartname, event, point, bkies){
    datasets = []
    for (i in event) {
        event_bk = event[i]
        bk = event_bk['bookmaker']  // string

        if (bkies.includes(bk)) {
            bk_odds = event_bk[point]  // list of ints
    //          console.log(bk_odds)

            datasets.push(setDataset(bk, bk_odds));
        }
    }

    var ctx = document.getElementById(chartname).getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: event[0]['time'],
            datasets: datasets
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            },
            elements: {
                    point:{
                        radius: 0
                    }
                }
        }
    });
}


function setDataset(title, odds){
    dataset = {
                label: title,
                data: odds,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }
    return dataset
}


function logSubmit(event) {
//    log.textContent = `Form Submitted! Time stamp: ${event.timeStamp}`;
//    event.preventDefault();
    location.reload();
}

const form = document.getElementById('form');
//  const log = document.getElementById('log');
form.addEventListener('submit', logSubmit);
