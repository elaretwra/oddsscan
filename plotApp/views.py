import json
import ftplib
from django.shortcuts import render
import pandas as pd
# from plotly.offline import plot
# from plotly.graph_objs import Scatter
import pickle
# from django.views.generic import TemplateView
# from chartjs.views.lines import BaseLineChartView
from rest_framework.views import APIView
from rest_framework.response import Response
from datetime import datetime
from os import getenv
import os
from google.cloud import storage
from django.core.cache import cache


def graphs(request, event_id):
    # x_data = [0, 1, 2, 3]
    # y_data = [x ** 2 for x in x_data]
    # plot_div = plot([Scatter(x=x_data, y=y_data,
    #                          mode='lines', name='test',
    #                          opacity=0.8, marker_color='green')],
    #                 output_type='div')
    # return render(request, "plotApp/index.html", context={'plot_div': plot_div})
    context = {
                'event_id': event_id
        }
    return render(request, 'plotApp/graphs.html', context)


def games(request):
    return render(request, 'plotApp/games.html')


def game(request, game_id):
    sport = 'football'
    # subfolder = 'SportsbooksOdds/' + sport + '/games/'
    # filename = 'game_upcoming_' + sport + '_key' + str(game_id) + '.json'
    # game_json = download_file_from_ftp(subfolder, filename)
    context = {
                'game_id': game_id
        }
    return render(request, 'plotApp/game.html', context)


def intro(request):
    return render(request, 'plotApp/intro.html')

#
# def Dashboard(request):
#
#     evs = Events.objects.using('wings').filter(localisation='Wing_1').order_by('-time')
#
#     time1 = []
#     evs = []
#
#     for dataset in evs:
#         time1.append(dataset.time.strftime("%Hh%M"))
#         temperature.append(dataset.temp_wing)
#
#     time1 = time1[:60]
#     temperature = temperature[:60]
#     time1.reverse()
#     temperature.reverse()
#
#     context = {
#                 'time1': time1,
#                 'temperature': temperature,
#     }
#     return render(request, 'Dashboard/dashboard.html', context)
#
#
# def index(request):
#     x_data = [0,1,2,3]
#     y_data = [x**2 for x in x_data]
#     plot_div = plot([Scatter(x=x_data, y=y_data,
#                         mode='lines', name='test',
#                         opacity=0.8, marker_color='green')],
#                output_type='div')
#     return render(request, "index.html", context={'plot_div': plot_div})


# class LineChartJSONView(BaseLineChartView):
#     def get_labels(self):
#         """Return 7 labels for the x-axis."""
#         return ["January", "February", "March", "April", "May", "June", "July"]
#
#     def get_providers(self):
#         """Return names of datasets."""
#         return ["Central", "Eastside", "Westside"]
#
#     def get_data(self):
#         """Return 3 datasets to plot."""
#
#         return [[75, 44, 92, 11, 44, 95, 35],
#                 [41, 92, 18, 3, 73, 87, 92],
#                 [87, 21, 94, 3, 90, 13, 65]]
#
#
# line_chart = TemplateView.as_view(template_name='line_chart.html')
# line_chart_json = LineChartJSONView.as_view()

def download_nread_file_from_bucket(storage_client, blob_name, file_path, bucket_name):
    market_df = None
    try:
        bucket = storage_client.get_bucket(bucket_name)

        blob = bucket.blob(blob_name)
        with open(file_path, 'wb') as f:
            storage_client.download_blob_to_file(blob, f)
        with open(file_path, 'rb') as f:
            market_df = pickle.load(f)

    except Exception as e:
        print(e)

    return market_df


def download_file_from_onedrive(blob_name):
    with open(getenv('OneDrive') + '\\' + blob_name + '.pkl', 'rb') as f:
        var_dwnld = pickle.load(f)

    return var_dwnld


def download_file_from_ftp(subfolder, filename):

    ftp = ftplib.FTP("192.168.1.7")
    ftp.login("orfan", "8426")
    ftp.cwd(subfolder)
    ftp.retrbinary("RETR " + filename, open(filename, 'wb').write)
    ftp.quit()

    with open(filename) as f:
        content = f.readlines()

    return content


class api_games(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        """
        Return a list of all users.
        """

        # cache django tutorial: https://docs.djangoproject.com/en/4.0/topics/cache/
        if cache.get('games') is None:
            sport = 'football'
            subfolder = 'SportsbooksOdds/' + sport + '/best_odds/'

            # Get the games in json format
            filename = 'market_best_upcoming_' + sport + '.json'
            games_json = download_file_from_ftp(subfolder, filename)

            # Get the leagues in json format
            filename = 'leagues_' + sport + '.json'
            leagues_json = download_file_from_ftp(subfolder, filename)

            # Cache the data in a raw format (not evaluated yet)
            data_raw = {'games': games_json[0], 'leagues': leagues_json[0]}
            cache.set_many(data_raw, 10)
        else:
            # Retrieve the cached data in a raw format (not evaluated yet)
            data_raw = cache.get_many(['games', 'leagues'])

        # Evaluate the raw data
        data = {'time_now': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
                'games': eval(data_raw['games']), 'leagues': eval(data_raw['leagues'])}

        return Response(data)


class api_game(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = []
    permission_classes = []

    def get(self, request, **kwargs):
        """
        Return a list of all users.
        """
        if cache.get('game') is None:
            sport = 'football'
            subfolder = 'SportsbooksOdds/' + sport + '/games/'

            # Get the game in json format
            filename = 'game_upcoming_' + sport + '_key' + str(kwargs['game_id']) + '.json'
            game_json = download_file_from_ftp(subfolder, filename)
            game_json = game_json[0]

            # Cache the game in a raw format (not evaluated yet)
            cache.set('game', game_json, 10)
        else:
            # Retrieve the cached game in a raw format (not evaluated yet)
            game_json = cache.get('game')

        data = {'time_now': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
                'game': eval(game_json)}

        return Response(data)