from django.shortcuts import render, redirect, get_object_or_404, Http404
from django.contrib.auth.decorators import login_required
from .forms import CustomUserCreationForm
from django.contrib.auth.models import User
from .models import Author
from oddsscan import helpers
from django.core.mail import send_mail
from django.contrib import messages


@login_required
def profile_info(request):

    return render(request, 'users/profile_info.html', context)


def register(request):
    # Email Verification, source:
    # https://overiq.com/django-1-10/django-extending-user-model/
    if request.method == 'POST':
        f = CustomUserCreationForm(request.POST)
        if f.is_valid():
            # send email verification now
            activation_key = helpers.generate_activation_key(username=request.POST['username'])
            subject = 'oddsscan Account Verification'
            message = '''\n
            Please visit the following link to verify your account \n\n{0}://{1}/activate/account/?key={2}
                                    '''.format(request.scheme, request.get_host(), activation_key)
            error = False

            try:
                send_mail(subject, message, None, [request.POST['email']])
                messages.add_message(request, messages.INFO, 'Account created! Click on the link sent to your email to activate the account')
            except:
                error = True
                messages.add_message(request, messages.INFO, 'Unable to send email verification. Please try again')

            if not error:
                u = User.objects.create_user(
                        request.POST['username'],
                        request.POST['email'],
                        request.POST['password1'],
                        is_active=0
                )
                author = Author()
                author.activation_key = activation_key
                author.user = u
                author.save()
            # messages.success(request, 'Account created successfully')
            # return redirect('register')
            return redirect('login')
    else:
        f = CustomUserCreationForm()

    return render(request, 'users/register.html', {'form': f})


def activate_account(request):
    key = request.GET['key']
    if not key:
        raise Http404()

    r = get_object_or_404(Author, activation_key=key, email_validated=False)
    r.user.is_active = True
    r.user.save()
    r.email_validated = True
    r.save()

    return render(request, 'users/activated.html')
